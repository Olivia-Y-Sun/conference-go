from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

# from django.shortcuts import render
from .acls import get_photo, get_weather


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
        "location",
    ]

    encoders = {
        "location": LocationListEncoder(),
    }

    # def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.
    """

    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    """
    Returns the details for the Conference model specified
    by the pk parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.
    """
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, pk):
    # conference = Conference.objects.get(id=pk)
    # return JsonResponse(
    #     conference, encoder=ConferenceDetailEncoder, safe=False
    # )

    if request.method == "GET":
        conference = Conference.objects.get(id=pk)
        # return JsonResponse(
        #     conference, encoder=ConferenceDetailEncoder, safe=False
        # )
        weather = get_weather(
            conference.location.city, conference.location.state
        )
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = conference.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    elif request.method == "PUT":
        # copied from create
        content = json.loads(request.body)
        # new code
        conference.objects.filter(id=pk).update(**content)

        # copied from get detail
        conference = conference.objects.get(id=pk)
        # weather = get_weather(conference.location.city, conference.location.state)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.
    """

    # locations = [
    #     {
    #         "name": l.name,
    #         "href": l.get_api_url(),
    #     }
    #     for l in Location.objects.all()
    # ]

    # return JsonResponse({"locations": locations})


# @require_http_methods(["GET", "POST"])
# def api_list_locations(request):
#     locations = Location.objects.all()
#     return JsonResponse(
#         {"locations": locations},
#         encoder=LocationListEncoder,
#     )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        # Find all locations
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        # Create a location
        # content = json.loads(request.body)
        # location = Location.objects.create(**content)
        # return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)

        content = json.loads(request.body)
        # get_photo
        picture_url_dict = get_photo(content["city"], content["state"])
        content.update(picture_url_dict)
        # Get the State object and put it in the content dict
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            # location = Location.objects.get(id=content["location"])
            # content["location"] = location
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    """
    Returns the details for the Location model specified
    by the pk parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.
    """

    # location_dict = {
    #     "name": location.name,
    #     "city": location.city,
    #     "room_count": location.room_count,
    #     "created": location.created,
    #     "updated": location.updated,
    #     "state": location.state,
    # }
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code
        Location.objects.filter(id=pk).update(**content)

        # copied from get detail
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
