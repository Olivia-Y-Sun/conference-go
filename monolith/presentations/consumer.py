import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
channel = connection.channel()

channel.queue_declare(queue="presentations")


def process_presentation_message(ch, method, properties, body):
    content = json.loads(body)
    print("presentations is processed", content)


channel.basic_consume(
    queue="presentations",
    auto_ack=True,
    on_message_callback=process_presentation_message,
)

print(" [*] Waiting for messages. To exit press CTRL+C")
channel.start_consuming()
