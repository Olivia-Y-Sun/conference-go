import json
import pika

# from pika.exceptions import AMQPConnectionError
import django
import os
import sys

# import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()


def process_approval(ch, method, properties, body):
    presentation = json.loads(body)
    send_mail(
        "Presentation approved",
        message="The following presentation was approved: "
        + presentation["title"],
        from_email="foo@bar.com",
        recipient_list=[presentation["presenter_email"]],
        fail_silently=False,
    )


channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)


def process_rejection(ch, method, properties, body):
    presentation = json.loads(body)
    send_mail(
        "Presentation rejected",
        message="The following presentation was rejected: "
        + presentation["title"],
        from_email="foo@bar.com",
        recipient_list=[presentation["presenter_email"]],
        fail_silently=False,
    )

channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)

channel.start_consuming()

# def process_approval(ch, method, properties, body):
#     content = json.loads(body)
#     # print("presentations is processed", content)
#     print("  Received %r" % content)

# 	send_mail(
# 	    "Your presentation has been accepted",
# 	    "f'{content[presenter_name]}', we're happy to tell you that your presentation f'{content[title]}' has been accepted.",
# 	    "from: someone@example.com",
# 	    "to: f'{content[presenter_email]}'",
# 	    fail_silently=False,
# 	)

# channel.basic_consume(
#     queue="presentations",
#     on_message_callback=process_approval,
#     auto_ack=True,
# )

# print(" [*] Waiting for messages. To exit press CTRL+C")
# channel.start_consuming()
