from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
# from events.api_views import ConferenceListEncoder
from common.json import ModelEncoder
from .models import AccountVO, Attendee, ConferenceVO


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }
    
    def get_extra_data(self, o):
        accountVO_count = AccountVO.objects.filter(email=o.email).count()
        dict = {}
        if accountVO_count > 0:
            dict["has_account"] = True
        else:
            dict["has_account"] = False

        return dict
      

        # Get the count of AccountVO objects with email equal to o.email
        # Return a dictionary with "has_account": True if count > 0
        # Otherwise, return a dictionary with "has_account": False


    # Lists the attendees names and the link to the attendee
    # for the specified conference id.

    # Returns a dictionary with a single key "attendees" which
    # is a list of attendee names and URLS. Each entry in the list
    # is a dictionary that contains the name of the attendee and
    # the link to the attendee's information.


# def api_list_attendees(request, conference_id):
#     attendees = [
#         {
#             "name": a.name,
#             "href": a.get_api_url(),
#         }
#         for a in Attendee.objects.all()
#     ]
#     return JsonResponse({"attendees": attendees})


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        # attendees = Attendee.objects.filter(conference=conference_id)
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)
        # Get the Conference object and put it in the content dict
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference

        # except Attendee.DoesNotExist:
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.
    """
    # attendee = Attendee.objects.get(id=pk)

    # attendee_dict = {
    #     "email": attendee.email,
    #     "name": attendee.name,
    #     "company_name": attendee.company_name,
    #     "created": attendee.created,
    #     "conference": {
    #         "name": attendee.conference.name,
    #         "href": attendee.conference.get_api_url(),
    #     },
    # }

    # return JsonResponse(
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.conference.name,
    #             "href": attendee.conference.get_api_url(),
    #         },
    #     }
    # )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        # copied from create
        content = json.loads(request.body)
        # new code
        attendee.objects.filter(id=pk).update(**content)

        # copied from get detail
        attendee = attendee.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
